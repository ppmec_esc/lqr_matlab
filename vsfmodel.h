#define V_ID (0)
#define YAW_RATE_ID (1)
#define ROLL_RATE_ID (2)
#define ROLL_ID (3)
#define U_ID (4)
#define OMEGA_ID (5)
#define WSPEED_ID (9)

#define NOF_STATES (9)

#define V          xC[V_ID]
#define YAW_RATE   xC[YAW_RATE_ID]
#define ROLL_RATE  xC[ROLL_RATE_ID]
#define ROLL       xC[ROLL_ID]
#define U          xC[U_ID]
#define OMEGA      (&xC[OMEGA_ID])
#define WSPEED      (&xC[WSPEED_ID])

#define LAST_V          outStates[V_ID]
#define LAST_YAW_RATE   outStates[YAW_RATE_ID]
#define LAST_ROLL_RATE  outStates[ROLL_RATE_ID]
#define LAST_ROLL       outStates[ROLL_ID]
#define LAST_U          outStates[U_ID]
#define LAST_OMEGA     (&outStates[OMEGA_ID])
#define LAST_WSPEED     (&outStates[WSPEED_ID])

#define FL (0)
#define FR (1)
#define RL (2)
#define RR (3)

#define MFA	  (&params[0])
#define MFB   (&params[8])
#define Z     (&params[17]) 
#define ZF0   (params[24]) 
#define ZR0   (params[25])
#define A     (params[26])
#define B     (params[27])
#define TF2   (params[28])
#define TR2   (params[29])
#define D     (&params[30])
#define REFF  (params[45])
#define LAT_ACCEL  ( dstates[V_ID] )
#define LONG_ACCEL ( dstates[U_ID] )

#define sind(x) (sin(fmod((x),360) * M_PI / 180))
#define tand(x) (tan(fmod((x),360) * M_PI / 180))
#define atand(x) (fmod(atan(x)*180/M_PI,360) )
#define atan2d(x,y) (fmod(atan2(x,y)*180/M_PI,360) )

real_T slip_angle[4] ={0,0,0,0};
real_T long_f[4] ={0,0,0,0};
real_T lat_f[4] ={0,0,0,0};
real_T load[4] ={0,0,0,0};
real_T dstates[4] = {0,0,0,0};
real_T long_slip[4] = {0,0,0,0};
