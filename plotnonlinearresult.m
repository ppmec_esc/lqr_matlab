 set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex');
set(0, 'DefaultLineLineWidth', 2);
figure(3)
subplot(2,2,1)
tempo = veh_states.Time;
slip_angle = side_slip.Data*180/pi;
plot( tempo,slip_angle );
hold on

title('Side-slip angle','FontSize',20)
xlabel('time(s)','FontSize',20)
ylabel('$\beta(deg)$','FontSize',20)

subplot(2,2,2)
yaw_rate = veh_states.Data(:,2)*180/pi;
plot( tempo, yaw_rate);
hold on
ref_yaw_rate = ref_states.Data(:,2)*180/pi;
plot( tempo, ref_yaw_rate);
title('Yaw rate','FontSize',20)
xlabel('time(s)','FontSize',20)
ylabel('$\dot\psi$(deg/s)','FontSize',20)

subplot(2,2,3)
roll = veh_states.Data(:,4)*180/pi;
plot( tempo, roll);
hold on
title('Roll angle','FontSize',20)
xlabel('time(s)','FontSize',20)
ylabel('$\phi$(deg)','FontSize',20)

subplot(2,2,4)
plot( yaw_moment_control_signal.Time, yaw_moment_control_signal.Data);
hold on
title('Command','FontSize',20)
xlabel('time(s)','FontSize',20)
ylabel('T(Nm)','FontSize',20)
