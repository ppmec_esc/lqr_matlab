set(0, 'DefaultLineLineWidth', 4);
set(0, 'defaultAxesTickLabelInterpreter','latex'); set(0, 'defaultLegendInterpreter','latex');

figure(3)
subplot(2,2,1)
tempo = timelog;
slip_angle = atan2(outlog(:,2),model_long_speed)*180/pi;
plot( tempo,slip_angle );
hold on

subplot(2,2,2)

aux = (l + Ku*control_long_speed*control_long_speed);
ref_yaw_rate = control_long_speed*outlog(:,6)/aux;
mu_g =  friction_coefficient*g;
ref_yaw_rate = min(abs(ref_yaw_rate), abs(mu_g/control_long_speed)).*sign(outlog(:,6));
ref_yaw_rate = ref_yaw_rate*180/pi;
plot( tempo, ref_yaw_rate);
hold on

yaw_rate = outlog(:,3)*180/pi;
plot( tempo, yaw_rate);

hold on

subplot(2,2,3)
roll = outlog(:,5)*180/pi;
plot( tempo, roll);
hold on

subplot(2,2,4)
plot( tempo, outlog(:,1));
hold on
