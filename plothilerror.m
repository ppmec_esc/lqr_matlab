set(0, 'DefaultLineLineWidth', 4);
set(0, 'defaultAxesTickLabelInterpreter','latex'); set(0, 'defaultLegendInterpreter','latex');

subplot(2,1,1)
tempo = timelog;
slip_angle = atan2(outlog(:,7),model_long_speed)*180/pi;
plot( tempo,(slip_angle) );
hold on

subplot(2,1,2)

aux = (l + Ku*control_long_speed*control_long_speed);
decsteer = steer(1:10:end,:);
ref_yaw_rate = control_long_speed*decsteer(:,2)/aux;
mu_g =  friction_coefficient*g;
ref_yaw_rate = min(abs(ref_yaw_rate), abs(mu_g/control_long_speed)).*sign(decsteer(:,2));
ref_yaw_rate = ref_yaw_rate*180/pi;
yaw_rate = outlog(:,8)*180/pi;
plot( decsteer(:,1), abs(yaw_rate-ref_yaw_rate));

hold on


