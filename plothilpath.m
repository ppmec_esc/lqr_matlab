tempo = timelog;
yaw_rate = outlog(:,3);
%L = filter(ones(101,1)/101,1,[yaw_rate' zeros(1,50)]);
fyaw_rate = yaw_rate;%L(51:end)';
fyaw_rate(abs(fyaw_rate)<0.03) = 0;
yaw_angle = cumtrapz(tempo,fyaw_rate);

lat_speed = outlog(:,2);
xspeed = model_long_speed.*cos(yaw_angle) - lat_speed.*sin(yaw_angle);
yspeed = model_long_speed.*sin(yaw_angle) + lat_speed.*cos(yaw_angle);
x = cumtrapz(tempo,xspeed);
y = cumtrapz(tempo,yspeed);

plot(x,y);